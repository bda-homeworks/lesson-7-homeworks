public class Company {
    String name;
    int budget;
    int totalSalary;

    public static double totalSalary(Employee[] isciler) {
        double totalSalary = 0;
        for (Employee isci : isciler) {
            totalSalary += isci.getSalary();
        }
        return totalSalary;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public void comparisonBudget(int totalSalary) {
        if (totalSalary > budget) {
            System.out.println("Iscilerin maasi umimi budceni asir");
        } else {
            System.out.println("Iscilerin sayi umimi budceni asmir!");
        }
    }
}
