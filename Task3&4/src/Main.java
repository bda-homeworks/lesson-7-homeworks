import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner number = new Scanner(System.in);
        System.out.println("Nece hesab yaranmagini isteyirsizse reqem secin:");
        int hesabsayi = number.nextInt();

        User[] users = new User[hesabsayi];

        for (int i = 0; i < hesabsayi; i++) {
            System.out.println("User" + (i + 1) + "- ucun ad yazin:");
            String name = number.next();
            System.out.println("User" + (i + 1) + "- ucun yasi qeyd edin:");
            int age = number.nextInt();
            System.out.println("User" + (i + 1) + "- ucun email yazin:");
            String email = number.next();
            System.out.println("User" + (i + 1) + "- ucun soyad yazin:");
            String surname = number.next();

            users[i] = new User(name, surname, email, age);
        }

        System.out.println("Butun hesablari cap etmek isteyirsizse - 'Yes', yox eger sistemden cixis etmek isteyirsize - 'No' yazin.");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine().toLowerCase();

        if (input.equals("yes")) {
            for (int j = 0; j < hesabsayi; j++) {
                User user = users[j];
                System.out.println("User" + (j + 1) + ":");
                System.out.println("Adi: " + user.getName());
                System.out.println("Soyadi: " + user.getSurname());
                System.out.println("Emaili:  " + user.getEmail());
                System.out.println("Yasi: " + user.getAge());
            }
        } else if (input.equals("no")) {
            System.exit(0);
        }
    }
}