
class Teacher {
    private String name;
    private String classField;

    public Teacher(String name, String classField) {
        this.name = name;
        this.classField = classField;
    }

    public String getName() {
        return name;
    }

    public String getClassField() {
        return classField;
    }
}
