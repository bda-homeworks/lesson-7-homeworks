import java.util.Scanner;

public class Student {
    private int age;
    private String faculty;
    private String university;
    private String name;
    private String surname;
    private Teacher teacher;

    public Student(int age, String faculty, String university, String name, String surname, Teacher teacher) {
        this.age = age;
        this.faculty = faculty;
        this.university = university;
        this.name = name;
        this.surname = surname;
        this.teacher = teacher;
    }

    public int getAge() {
        return age;
    }

    public String getFaculty() {
        return faculty;
    }

    public String getUniversity() {
        return university;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }
}
