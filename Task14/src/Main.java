import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the number of students: ");
        int numStudents = sc.nextInt();
        sc.nextLine();

        Student[] students = new Student[numStudents];

        for (int i = 0; i < numStudents; i++) {
            System.out.println("\nEnter details for student " + (i + 1) + ":");
            System.out.print("Age: ");
            int age = sc.nextInt();
            sc.nextLine();
            System.out.print("Faculty: ");
            String faculty = sc.nextLine();
            System.out.print("University: ");
            String university = sc.nextLine();
            System.out.print("Name: ");
            String name = sc.nextLine();
            System.out.print("Surname: ");
            String surname = sc.nextLine();
            System.out.print("Teacher name: ");
            String teacherName = sc.nextLine();
            System.out.print("Teacher class: ");
            String teacherClass = sc.nextLine();
            Teacher teacher = new Teacher(teacherName, teacherClass);

            students[i] = new Student(age, faculty, university, name, surname, teacher);
        }

        System.out.print("\nEnter the number of teachers: ");
        int numTeachers = sc.nextInt();
        sc.nextLine();

        Teacher[] teachers = new Teacher[numTeachers];

        for (int i = 0; i < numTeachers; i++) {
            System.out.println("\nEnter details for teacher " + (i + 1) + ":");
            System.out.print("Name: ");
            String name = sc.nextLine();
            System.out.print("Class: ");
            String classField = sc.nextLine();

            teachers[i] = new Teacher(name, classField);
        }

        System.out.println("\nDetails of the students:");

        for (int i = 0; i < numStudents; i++) {
            Student student = students[i];
            Teacher teacher = student.getTeacher();

            System.out.println("\nName: " + student.getName() + " " + student.getSurname());
            System.out.println("Age: " + student.getAge());
            System.out.println("Faculty: " + student.getFaculty());
            System.out.println("University: " + student.getUniversity());
            System.out.println("Teacher Name: " + teacher.getName());
            System.out.println("Teacher Class: " + teacher.getClassField());
        }
    }
}