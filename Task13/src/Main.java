import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Nece student yaratmaq istediyinizi qeyd edin:");
        Scanner input1 = new Scanner(System.in);
        int n = input1.nextInt();

        Student[] students = new Student[n];

        for (int i = 0; i < students.length; i++) {
            System.out.println((i + 1) + "-ci telebe ucun melumatlari qeyd edin:");
            System.out.println("Ad:");
            String name = input1.next();
            System.out.println("Soyad:");
            String surname = input1.next();
            System.out.println("Universitet:");
            String university = input1.next();
            System.out.println("Fakultet:");
            String faculty = input1.next();
            System.out.println("Yas:");
            int age = input1.nextInt();

            students[i] = new Student(name, surname, age, faculty, university);
        }
        for (int i = 0; i < students.length; i++) {
            System.out.println((i + 1) + "-ci telebe barede melumat");
            System.out.println("Ad - " + students[i].name);
            System.out.println("Soyad - " + students[i].surname);
            System.out.println("Yas - " + students[i].age);
            System.out.println("Universitet - " + students[i].university);
            System.out.println("Fakultet - " + students[i].faculty);
        }
    }
}