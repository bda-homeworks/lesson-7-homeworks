public class Student {
    String name;
    String surname;
    int age;
    String university;
    String faculty;

    public Student(String name, String surname, int age, String university, String faculty) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.university = university;
        this.faculty = faculty;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public String getUniversity() {
        return university;
    }

    public String getFaculty() {
        return faculty;
    }
}
