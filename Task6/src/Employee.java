public class Employee {
    String name;
    String surname;
    String position;
    int age;
    int salary;

    public Employee() {
    }

    public Employee(String name, String surname, String position, int age, int salary) {
        this.name = name;
        this.surname = surname;
        this.position = position;
        this.age = age;
        this.salary = salary;
    }

    // BURDAN ASAGIDAKILARI DEYISMEK UCUNDUR

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    // BURDAN ASAGIDAKILAR ELDE ETMEK UCUN

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPosition() {
        return position;
    }

    public int getAge() {
        return age;
    }

    public int getSalary() {
        return salary;
    }
}
