import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Isci barede melumat qeyd edin: Ad soyad vezife yas ve maas ardicilligi ile:");
        Employee isci = new Employee(input.next(), input.next(), input.next(), input.nextInt(), input.nextInt());

        System.out.println(isci.name + "," + isci.surname + "," + isci.position + "," + isci.age + "," + isci.salary);
        String cavab;

        do {
            System.out.println("Deyismek istediyinizi yazin: 1-Ad 2-Soyad 3-Vezife 4-Yas yada 5-Maas");
            int deyismek = input.nextInt();
            switch (deyismek) {
                case 1:
                    isci.setName(input.next());
                    System.out.println(isci.name + "," + isci.surname + "," + isci.position + "," + isci.age + "," + isci.salary);
                    break;
                case 2:
                    isci.setSurname(input.next());
                    System.out.println(isci.name + "," + isci.surname + "," + isci.position + "," + isci.age + "," + isci.salary);
                    break;
                case 3:
                    isci.setPosition(input.next());
                    System.out.println(isci.name + "," + isci.surname + "," + isci.position + "," + isci.age + "," + isci.salary);
                    break;
                case 4:
                    isci.setAge(input.nextInt());
                    System.out.println(isci.name + "," + isci.surname + "," + isci.position + "," + isci.age + "," + isci.salary);
                    break;
                case 5:
                    isci.setSalary(input.nextInt());
                    System.out.println(isci.name + "," + isci.surname + "," + isci.position + "," + isci.age + "," + isci.salary);
                    break;
                default:
                    System.out.println("Bele secim yoxdur!");
                    break;
            }

            System.out.println("Yene neyise deyismek isteyirsiz? (he/yox)");
            cavab = input.next();

        } while (cavab.equalsIgnoreCase("he"));
        System.out.println("Sagolun!");
    }
}