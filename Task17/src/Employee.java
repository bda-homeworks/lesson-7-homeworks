public class Employee {
    String name;
    String surname;
    String department;
    String position;
    int salary;

    public Employee() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Employee(String name, String surname, String department, String position, int salary) {
        System.out.println("Adiniz: " + name);
        System.out.println("Soyadiniz: " + surname);
        System.out.println("Bolmeniz: " + department);
        System.out.println("Vezifeniz: " + position);
        System.out.println("Maasiniz: " + salary);
        this.name = name;
        this.surname = surname;
        this.department = department;
        this.position = position;
        this.salary = salary;
    }
    public void methodPrintAll () {
        System.out.println(name);
        System.out.println(surname);
        System.out.println(department);
        System.out.println(position);
        System.out.println(salary);
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getDepartment() {
        return department;
    }

    public String getPosition() {
        return position;
    }

    public int getSalary() {
        return salary;
    }
}
