import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("1- Adinizi , 2- Soyadinizi, 3-Bolmenizi, 4-Vezifenizi, 5-Maasinizi qeyd etmeyi unutmayin!");
        Employee isciler = new Employee(input.next(), input.next(), input.next(), input.next(), input.nextInt());

        String answer;
        do {
            System.out.println("1. Iscinin adini cap etmek ucun");
            System.out.println("2. Iscinin soyadini cap etmek ucun");
            System.out.println("3. Iscinin bolmesini cap etmek ucun");
            System.out.println("4. Iscinin vezifesini cap etmek ucun");
            System.out.println("5. Iscinin maasini cap etmek ucun");
            System.out.println("6. Iscinin butun melumatlari");
            System.out.println("7. Iscinin maasini deyisdirmek");
            System.out.println("8. Iscinin vezifini deyisdirmek");
            int secim = input.nextInt();
            switch (secim) {
                case 1:
                    String name = isciler.name;
                    System.out.println("Iscinin adi: " + name);
                    break;
                case 2:
                    String surname = isciler.surname;
                    System.out.println("Iscinin soyadi: " + surname);
                    break;
                case 3:
                    String department = isciler.department;
                    System.out.println("Iscinin bolmesi: " + department);
                    break;
                case 4:
                    String position = isciler.position;
                    System.out.println("Iscinin vezifesi: " + position);
                    break;
                case 5:
                    int salary = isciler.salary;
                    System.out.println("Iscinin maasi: " + salary);
                    break;
                case 6:
                    System.out.println("Butun melumatlar");
                    isciler.methodPrintAll();
                case 7:
                    System.out.println("Iscinin deyisilmis maasini qeyd edin:");
                    isciler.setSalary(input.nextInt());
                    int salarychanged = isciler.salary;
                    System.out.println(salarychanged);
                    break;
                case 8:
                    System.out.println("Iscinin vezifesini deyisdirib yeni vezifeni qeyd edin:");
                    isciler.setPosition(input.next());
                    String yenivezife = isciler.position;
                    System.out.println(yenivezife);
                    break;
                default:
                    System.out.println("Bele bir secim yoxdur!");
            }
            System.out.print("Do you want to try again? (yes/no): ");
            answer = input.next();
        } while (answer.equalsIgnoreCase("yes"));
        System.out.println("Goodbye!");
    }
}