public class Lamp {
    boolean condition;

    public Lamp() {
        this.condition = false;
    }

    public void setCondition(boolean condition) {
        this.condition = condition;
    }

    public boolean isCondition() {
        return condition;
    }
}
