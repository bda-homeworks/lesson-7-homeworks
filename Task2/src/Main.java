import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Nece hesab yaranmagini isteyirsizse reqem secin:");
        int hesabsayi = input.nextInt();

        User[] users = new User[hesabsayi];

        for (int i = 0; i < hesabsayi; i++) {
            System.out.println("User" + (i + 1) + "- ucun ad yazin:");
            String name = input.next();
            System.out.println("User" + (i + 1) + "- ucun yasi qeyd edin:");
            int age = input.nextInt();
            System.out.println("User" + (i + 1) + "- ucun email yazin:");
            String email = input.next();
            System.out.println("User" + (i + 1) + "- ucun soyad yazin:");
            String surname = input.next();

            users[i] = new User(name, surname, email, age);
        }

    }
}