public class Customer {
    String name;
    double balance;

    public Customer(String name, double balance) {
        this.name = name;
        this.balance = balance;
        System.out.println(name + " has $" + balance);
    }

    public String getName() {
        return name;
    }

    public double getBalance() {
        return balance;
    }

    public void transferMoney (Customer recipient, double amount) {
        if (amount <= balance) {
            balance = balance - amount;
            recipient.balance += amount;
            System.out.println(name + " transferred $" + amount + " to " + recipient.getName());
        } else {
            System.out.println("Insufficent funds");
        }
    }
}
