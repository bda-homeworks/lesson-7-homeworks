import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Customer customer1 = new Customer("John", 1000);
        Customer customer2 = new Customer("Jane", 500);

        System.out.println("Enter the amount to transfer from " + customer1.name + " to " + customer2.name);
        Scanner input = new Scanner(System.in);
        customer1.transferMoney(customer2, input.nextInt());

        System.out.println(customer1.getName() + " balance: $" + customer1.getBalance());
        System.out.println(customer2.getName() + " balance: $" + customer2.getBalance());
    }
}