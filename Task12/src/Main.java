import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("");
        Student telebe = new Student();
        Teacher muellim = new Teacher();

        System.out.println("Telebenin adini qeyd edin");
        telebe.setName(input.next());
        System.out.println("Telebenin soyadini qeyd edin");
        telebe.setSurname(input.next());
        System.out.println("Telebenin yasini qeyd edin");
        telebe.setAge(input.nextInt());
        System.out.println("Telebenin fakultesini qeyd edin qeyd edin");
        telebe.setFaculty(input.next());


        System.out.println("Muellimin adini qeyd edin");
        muellim.setName(input.next());
        System.out.println("Muellimin soyadini qeyd edin");
        muellim.setSurname(input.next());
        System.out.println("Muellimin yasini qeyd edin");
        muellim.setAge(input.nextInt());
        System.out.println("Muellimin fakultesini qeyd edin qeyd edin");
        muellim.setFaculty(input.next());



        while (true) {
            System.out.println("Muellimin adi ucun -1, Soyadi ucun -2, Yasi ucun -3, Fakultesi ucun -4, Sistemden cixis etmek ucun ise -5,-e vurun.");
            Scanner scanner = new Scanner(System.in);
            int nomre = scanner.nextInt();

            switch (nomre) {
                case 1:
                    System.out.println("Muellimin adi:");
                    muellim.getName();
                    System.out.println("Telebenin adi:");
                    telebe.getName();
                    break;
                case 2:
                    System.out.println("Muellimin soyadi:");
                    muellim.getSurname();
                    System.out.println("Telebenin soyadi:");
                    telebe.getSurname();
                    break;
                case 3:
                    System.out.println("Muellimin yasi:");
                    muellim.getAge();
                    System.out.println("Telebenin yasi:");
                    telebe.getAge();
                    break;
                case 4:
                    System.out.println("Muellimin fakultesi:");
                    muellim.getFaculty();
                    System.out.println("Telebenin fakultesi:");
                    telebe.getFaculty();
                    break;
                case 5:
                    System.exit(0);
                default:
                    System.out.println("Bele bir secim yoxdur");
            }
        }
    }
}