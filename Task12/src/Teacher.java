public class Teacher {
    String name;
    String surname;
    int age;
    String faculty;

    public Teacher() {
    }
    public Teacher(String name, String surname, int age, String faculty) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.faculty = faculty;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public void getName() {
        System.out.println(this.name);
    }

    public void getSurname() {
        System.out.println(this.surname);
    }

    public void getAge() {
        System.out.println(this.age);
    }

    public void getFaculty() {
        System.out.println(this.faculty);
    }
}
