import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Telebenin melumatlarini qeyd edin 1.Ad, 2.Soyad, 3.Universitet, 4.Ixtisas");
        Student telebe = new Student(input.next(), input.next(), input.next(), input.next());

        System.out.println("Dogum tarixini qeyd etmek isteyirsiz? yes/no (optional):");
        String cavab = input.next();

        if (cavab.equalsIgnoreCase("yes")) {
            System.out.println("Telebenin doguldugu tarixi qeyd edin:");
            String bornDate = input.next();
            telebe.borndate = bornDate;
        }
        String answer;
        do {
            System.out.println("1.Adi cap etmek");
            System.out.println("2.Soyadi cap etmek");
            System.out.println("3.Universiteti cap etmek");
            System.out.println("4.Ixtisasi cap etmek");
            System.out.println("5.Dogum tarixini cap etmek");
            int secim = input.nextInt();
            switch (secim) {
                case 1:
                    System.out.println("Adi cap etmek:");
                    String ad = telebe.name;
                    System.out.println(ad);
                    break;
                    case 2:
                    System.out.println("Soyadi cap etmek:");
                    String soyad = telebe.surname;
                    System.out.println(soyad);
                    break;
                    case 3:
                    System.out.println("Universiteti cap etmek:");
                    String universitet = telebe.university;
                    System.out.println(universitet);
                    break;
                    case 4:
                    System.out.println("Ixtisasi cap etmek:");
                    String ixtisas = telebe.speciality;
                    System.out.println(ixtisas);
                    break;
                case 5:
                    System.out.println("Dogum tarixini cap etmek");
                    String dogumtarixi = telebe.borndate;
                    System.out.println(dogumtarixi);
                    break;
                default:
                    System.out.println("Bele bir reqem yoxdur!");
            }

            System.out.println("Do you want to try again? yes/no");
            answer = input.next();
        } while (answer.equalsIgnoreCase("yes"));
        System.out.println("Goodbye!");
    }
}