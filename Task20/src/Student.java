public class Student {
    String borndate;
    String name;
    String surname;
    String university;
    String speciality;

    public Student() {
    }

    public Student(String name, String surname, String university, String speciality) {
        this.name = name;
        this.surname = surname;
        this.university = university;
        this.speciality = speciality;
    }
    public void changedate (Double time) {
        this.borndate = borndate;
    }

    public String getBorndate() {
        return borndate;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUniversity() {
        return university;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void getAllInfo () {
        System.out.println(name);
        System.out.println(surname);
        System.out.println(university);
        System.out.println(speciality);
    }
}
