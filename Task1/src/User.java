public class User {
    String name;
    String surname;
    int age;
    String email;

    public User(String name, String surname, String email, int age) {
        System.out.println("Adiniz: " + name);
        System.out.println("Soyadiniz: " + surname);
        System.out.println("Yasiniz: " + age);
        System.out.println("Emailiniz: " + email);
        this.email = email;
        this.surname = surname;
        this.name = name;
        this.age = age;
    }
}
